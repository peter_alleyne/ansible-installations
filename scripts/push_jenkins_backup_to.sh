
#!/bin/bash

NEXUS_SERVER="http://nexus-banq.slant.com:8081"
JENKINS_BACKUP_DIR="/var/jenkins_backups"

BACKUP_LIST=`ls ${JENKINS_BACKUP_DIR}`


for item in ${BACKUP_LIST} 
do
     if [ -d "${JENKINS_BACKUP_DIR}/${item}" ]; then 
          echo "${item}"
          tar cfz "${JENKINS_BACKUP_DIR}/${item}.tar.gz" "${JENKINS_BACKUP_DIR}/${item}"
          curl -v -u palleyne:welcome --upload-file "${JENKINS_BACKUP_DIR}/${item}.tar.gz" "${NEXUS_SERVER}/repository/Application-Backups/Jenkins-Backup/${item}.tar.gz"
          rm -f "${JENKINS_BACKUP_DIR}/${item}.tar.gz"
     fi
done


