#! /bin/bash

if [ "$#" -ne 1 ]
then
  echo "Error: No domain name argument provided"
  echo "Usage: Provide a domain name as an argument"
  exit 1
fi

DOMAIN=$1

# Create root CA & Private key

openssl req -x509 \
            -sha256 -days 356 \
            -nodes \
            -newkey rsa:2048 \
            -subj "/CN={{ domainName }}/C={{ country }}/L={{ locality }}" \
            -keyout rootCA.key -out rootCA.crt 

# Generate Private key 

openssl genrsa -out {{ domainName }}.key 2048

# Create csf conf

cat > csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = {{ country }}
ST = {{ state }}
L = {{ locality }}
O = {{ organization }}
OU = {{ organizationalUnit }}
CN = {{ commonName }}

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = {{ commonName }}
IP.1 = {{ ipAddress }} 

EOF

# create CSR request using private key

openssl req -new -key {{ commonName }}.key -out {{ commonName }}.csr -config csr.conf

# Create a external config file for the certificate

cat > cert.conf <<EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = {{ commonName }}

EOF

# Create SSl with self signed CA

openssl x509 -req \
    -in {{ commonName }}.csr \
    -CA rootCA.crt -CAkey rootCA.key \
    -CAcreateserial -out {{ commonName }}.crt \
    -days 365 \
    -sha256 -extfile cert.conf \
    -extfile v3.ext

# Create the key store for jenkins
# Convert SSL keys to PKCS12 format

openssl pkcs12 -export -out keystore.p12 \
-passout 'pass:{{ nexus_https_keystore_password }}' -inkey {{ commonName }}.key \
-in {{ commonName }}.crt -certfile rootCA.crt -name {{ commonName }}

# Convert PKCS12 to JKS format

keytool -importkeystore -srckeystore keystore.p12 \
-srcstorepass '{{ nexus_https_keystore_password }}' -srcstoretype PKCS12 \
-srcalias {{ commonName }} -deststoretype JKS \
-destkeystore keystore.jks -deststorepass '{{ nexus_https_keystore_password }}' \
-destalias {{ commonName }}


# Add JKS to keystore path

mkdir -p /var/nexus/sonatype-work/nexus3/etc/ssl

cp keystore.jks /var/nexus/sonatype-work/nexus3/etc/ssl

chown -R nexus: /var/nexus/sonatype-work/nexus3/etc/ssl
chmod 755 /var/nexus/sonatype-work/nexus3/etc/ssl
chmod 655 /var/nexus/sonatype-work/nexus3/etc/ssl/keystore.jks

# Modify Nexus Configuration for SSL

# Add CA cert to keystore

keytool  -import  -trustcacerts \
         -alias certAlias -file rootCA.crt \
         -storepass '{{ nexus_https_keystore_password }}' \
         -noprompt \
         -keystore /var/nexus/sonatype-work/nexus3/etc/ssl/keystore.jks

 
