#!/bin/bash

DATASOURCES=`ls ${WORKSPACE}/resources/idel*`

# Load the datasources into the Nexus repository
for item in ${DATASOURCES}; do
    VALUE=$(basename $item)
    ARTIFACTID=$(t=$(basename $item) && echo -n ${t%.*})
    curl -X PUT -v -u ${NEXUS_USER}:${NEXUS_PASS} --upload-file "${WORKSPACE}/resources/${VALUE}" "http://${NEXUS_REGISTRY_ADDRESS}:8081/repository/maven-snapshots/ca/qc/banq/gia/${ARTIFACTID}/${VERSION}-SNAPSHOT/${ARTIFACTID}-${SNAPSHOT_VERSION}-1.xml"
done
