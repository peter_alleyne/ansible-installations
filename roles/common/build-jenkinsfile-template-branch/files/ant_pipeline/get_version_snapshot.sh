#!/bin/bash

MY_VERSION=$(grep "<property name=\"project.version\"" ${WORKSPACE}/build.xml | sed "s/<property name=\"project.version\".*=\"\([0-9]\.[0-9]\+.[0-9]\+.*\)\"\/>/\1/g" | xargs)

echo -n "${MY_VERSION}"
