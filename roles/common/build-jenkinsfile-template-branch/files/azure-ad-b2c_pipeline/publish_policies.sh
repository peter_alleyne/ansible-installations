#!/bin/bash

POLICIES=`ls ${WORKSPACE}/banq-gia/*`

# Load the policies into the Nexus repository
for item in ${POLICIES}; do
    VALUE=$(basename $item)
    ARTIFACTID=$(t=$(basename $item) && echo -n ${t%.*})
    curl -X PUT -v -u ${NEXUS_USER}:${NEXUS_PASS} --upload-file "${WORKSPACE}/banq-gia/${VALUE}" "http://${NEXUS_REGISTRY_ADDRESS}:8081/repository/maven-snapshots/ca/qc/banq/gia/${PROJECT_NAME}/${ARTIFACTID}/${VERSION}-SNAPSHOT/${ARTIFACTID}-${SNAPSHOT_VERSION}-1.xml"
done
