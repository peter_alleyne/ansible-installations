
#!/bin/bash

# Update the build.xml file for a release...


sed -i "s|<property name=\"project.version\".*|<property name=\"project.version\" value=\"${DEVELOPMENT_VERSION}\"/>|g" ${WORKSPACE}/build.xml
