#!/bin/bash

POLICIES=`ls ../banq-gia/*`

echo ""
echo "Syntax checking files..."

for item in ${POLICIES};
do
   xmllint $item --noout

   if [ $? -ne 0 ]; then
      exit $?
   else
      echo "$item OK..."
   fi
done

echo ""
echo "Validating files..."

for item in ${POLICIES};
do
   if [ "${item}" != "../banq-gia/TrustFrameworkBase.xml" ]; then
      xmllint --schema ../TrustFrameworkPolicy_0.3.0.0.xsd $item --noout
   fi

   if [ $? -ne 0 ]; then
      exit $?
   fi
done

