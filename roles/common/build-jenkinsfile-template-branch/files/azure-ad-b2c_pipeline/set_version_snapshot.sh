#!/bin/bash


echo "Shell = $SHELL"

sed -r -i 's|<property name=\"project.version\".*=\"([0-9].*)\".*?|<property name=\"project.version\" value=\"\1-'$(date +%Y%m%d.%H%M%S)'\"/>|' ${WORKSPACE}/build.xml
