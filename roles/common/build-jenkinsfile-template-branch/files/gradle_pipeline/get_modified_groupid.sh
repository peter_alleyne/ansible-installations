#!/bin/bash
###########


groupid=$1

# The next development version.
groupid_mod=$(echo ${groupid} | sed 's/\./\//g')

# The return value
echo -n $groupid_mod
