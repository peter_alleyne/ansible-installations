
#!/bin/bash


echo "SHELL                 ======  ${SHELL}"
echo "VERSION               ======  ${VERSION}"
echo "DEVELOPMENT_VERSION   ====== ${DEVELOPMENT_VERSION}"


# Update the build.gradle file for a release...

sed -i "s/^version\s.*\([0-9]\.[0-9]\+\.[0-9]\+\).*/version ${VERSION}/g" ${WORKSPACE}/build.gradle
