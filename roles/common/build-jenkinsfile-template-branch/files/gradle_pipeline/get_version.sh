#!/bin/bash


MY_VERSION=$(grep "^version.*$" ${WORKSPACE}/build.gradle | sed "s/^version\s.*\([0-9]\.[0-9]\+\.[0-9]\+\).*/'\1\'/g")

echo -n "${MY_VERSION}"


