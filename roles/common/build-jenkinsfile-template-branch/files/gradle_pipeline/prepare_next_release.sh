
#!/bin/bash

# Update the build.gradle file for a release...

sed -i "s/^version\s.*\([0-9]\.[0-9]\+\.[0-9]\+\).*/version '${DEVELOPMENT_VERSION}'/g" ${WORKSPACE}/build.gradle
