#!/bin/bash

current_version=$1

# Set the split version based on a '.' delimiter
prefix=${current_version%.*}
suffex=$((${current_version##*.}+1))

#while [[ ${#suffex} -lt 2 ]] ; do
#	suffex="0${suffex}"
#done

# The next development version.
development_version=${prefix}.${suffex}-SNAPSHOT

# The return value
echo -n "$development_version"
