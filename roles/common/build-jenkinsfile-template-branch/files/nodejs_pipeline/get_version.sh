#!/bin/bash


MY_VERSION=$(grep "\"version\":" ${WORKSPACE}/package.json | sed "s/.*\([0-9]\.[0-9]\+\.[0-9]\+\).*/'\1\'/g")

echo -n "${MY_VERSION}"
