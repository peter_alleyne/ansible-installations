
#!/bin/bash

# Update the build.gradle file for a release...

sed -i "s/\"version\":.*/\"version\": \"${DEVELOPMENT_VERSION}\",/g" package.json
