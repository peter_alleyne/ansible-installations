#!/bin/bash


echo "Shell = $SHELL"

sed -r -i 's|\"version\":.*\"([0-9].*)\".*?|\"version\": \"\1-SNAPSHOT-'$(date +%Y%m%d.%H%M%S)'\",|' ${WORKSPACE}/package.json
