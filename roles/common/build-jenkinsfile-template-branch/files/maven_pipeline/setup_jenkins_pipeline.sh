#!/bin/bash

# Check if the local branch exists...

exists_in_local=$(git branch --list ${LOCAL_BRANCH})

if [ -z "$exists_in_local" ]; then
    echo "The local branch ${LOCAL_BRANCH} does not exist..."
else
    echo "Delete the local branch ${LOCAL_BRANCH}..."
    git branch -d ${LOCA__BRANCH}
fi

echo "Creating a fresh local branch ${LOCAL_BRANCH}"
git branch -b ${LOCAL_BRANCH}

# Get all of the tags..
echo "Fetching the tags..."
git fetch --tags

# Check if the remote template branch exists.

exists_in_remote=$(git ls-remote --heads origin ${REMOTE_BRANCH})

if [ -z "$exists_in_remote" ]; then
    echo "The remote branch ${REMOTE_BRANCH} does not exist..."
else
    echo "Delete the remote branch ${REMOTE_BRANCH}..."
    git push origin --delete ${REMOTE_BRANCH}
fi
