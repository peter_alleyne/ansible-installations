#!/bin/bash


set -x

echo "Killing the running intranet process..."

PROCESS=$(ps -ef | grep ideln-backend/intranet | grep -v grep | awk '{print $2}')

if [ -n "$PROCESS" ]
then
      echo "Killing intranet process ${PROCESS}..."
      kill -9 $PROCESS
else
      echo "No intranet process to kill..."
fi
