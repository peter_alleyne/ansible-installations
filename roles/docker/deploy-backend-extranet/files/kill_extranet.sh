#!/bin/bash


set -x

echo "Killing the running extranet process..."

PROCESS=$(ps -ef | grep ideln-backend/extranet | grep -v grep | awk '{print $2}')

if [ -n "$PROCESS" ]
then
      echo "Killing extranet process ${PROCESS}..."
      kill -9 $PROCESS
else
      echo "No extranet process to kill..."
fi
